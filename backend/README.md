# Backend

Um server feito em NodeJS com um banco de dados PostegresSQL.

Uma roda de login onde recebe um usuário e um senha e responde com o nome e código de autorização do usuário se usuário existir e a senha estiver correta.

O código de autorização é 1 para administradores e 2 para usuários comuns.


#### Exemplo de request
```json
{
    "username": "user",
    "password": "password"
}
```

#### Exemplo de response
```json
{
    "name": "User Name",
    "authcode": 2
}
```

## Para rodar

### Alterar senha do postgres e criar Banco de dados
----------------------------------------------------

```
$ sudo -s
$ su postgres
$ service postgresql start
$ psql
$ ALTER USER postgres PASSWORD 'password';
$ CREATE DATABASE 'database_development';
$ \q
$ exit
```
### Para criar as tabelas e popular o banco
-------------------------------------------

Certifique-se que tem instalado a Sequelize CLI (Command Line Interface) instalado globalmente para os proximos passos. Para instala-lo simplismente:
```
$ npm install --global sequelize-cli
```

Para migrar e alimentar as tabelas do banco
```
$ sequelize db:migrate
$ sequelize db:seed:all
```

Ao fim vai ser criada uma tabla `user` com dois usuário inseridos.

#### Usuários

```json
{
    "username": "user",
    "password": "User1234",
    "name": "Lucas Lima",
    "authcode": 2
}
```
```json
{
    "username": "admin",
    "password": "Admin1234",
    "name": "Maria Ferreira",
    "authcode": 1
}

```
### Instalando dependencias e rodando
-------------------------------------

Antes de rodar certifique-se que tem nodemon instalado globalmente.  

Para instalar nodemon globalmente  

```
$ npm install --global nodemon
```

#### Com npm
```
$ npm install
$ npm start
```

#### Com Yarn
```
$ yarn install
$ yarn start
```