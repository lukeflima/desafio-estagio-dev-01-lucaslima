const jwt = require('jsonwebtoken');
const bcrypt = require ('bcrypt');
const userModel = require('../models').user;

const saltRounds = 10

module.exports = {
    async login(req, res){
        const { username, password } = req.body
        console.log(userModel)
        if(username && password){
            try{
                // Querys db for user
                const user = await userModel.findOne({
                    attributes: ['name', 'password', 'authcode'],
                    where: { username },
                })
                const passworddb = user.dataValues.password
                const {name, authcode} = user.dataValues
                if (await bcrypt.compare(password, passworddb)){    // Compares if password match
                    //Create token and sends as response
                    const token = await jwt.sign ({authcode, name}, 'secretkey')
                    res.json({ token })
                } else{
                    res.status(403).send({ response: "Wrong user or password" })
                }
            } catch(e){
                console.log(e)
                res.status(403).send({ response: "Wrong user or password" })
            }
        }
        else{
            res.status(403).send({ response: "Empty fields" })
        }
    }
}