'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user', [{
      username: "admin",
      name:     "Maria Ferreira",
      password: "$2b$10$MyIJo8Mr3RxZHYmw/MgulO4N/hjhnxy22w5hqjSpNy5JgaeCrdYmW",
      authcode: 1,
    },
    {
      username: "user",
      name:     "Lucas Lima",
      password: "$2b$10$k.xZjVMYJPhLybs8icdjB.YAsVVrvqf5gJtX6CLlAtPy6mQwZHHMG",
      authcode: 2,
    },
  ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user', null, {});
  }
};
