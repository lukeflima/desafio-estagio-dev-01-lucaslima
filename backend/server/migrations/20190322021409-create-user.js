'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user', {
        username: {
            allowNull: false,
            type: Sequelize.STRING(255),
            unique: true,
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING(255),
          unique: true,
      },
        password: {
            allowNull: false,
            type: Sequelize.STRING(255)
        },
        authcode: {
          allowNull: false,
          type: Sequelize.INTEGER
        },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user');
  }
};