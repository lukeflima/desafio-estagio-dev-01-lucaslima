const jwt = require('jsonwebtoken');

module.exports = {
    async verifyToken(req, res, next) {

    console.log(req.headers['authorization'])
    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader !== 'undefined') {
        //Extract token
        const bearer = bearerHeader.split(' ');
        const token = bearer[1];
        try{
            //Send authCode if token is valid
            const { authCode } = await jwt.verify(token, 'secretkey')
            res.status(200).send({ authCode });
        }catch(e){
            //Error out for invalid token
            res.status(403).send({ response: "Invalid token" });
        }
    }
  },
}