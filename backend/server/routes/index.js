const auth = require('./auth');
const loginController = require('../controllers/login');

const PORT = 5000

module.exports = async (app) => {
    app.get('/', auth.verifyToken);

    app.post('/login', loginController.login);
}