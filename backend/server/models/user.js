'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    username: {
      type: DataTypes.STRING(255),
      unique: true,
      validate: {
        len: [0,255],
      }
    },
    name: {
      type: DataTypes.STRING(255),
      unique: true,
      validate: {
        len: [0,255],
      }
    },
    password: {
      type: DataTypes.STRING(255),
      validate: {
        len: [0,255],
      }
    },
    authcode: {
      type: DataTypes.INTEGER,
      validate: {
        isIn: [[1,2]]
      }
    },
  },
   {
      freezeTableName: true,
  });
  return user
}