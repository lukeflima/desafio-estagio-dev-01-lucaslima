const express = require('express');
const bodyParser = require('body-parser');

var cors = require('cors');

// Set up the express app
const app = express();

app.use(cors())

//app.use(bodyParser.text({ type: 'text/plain' }))
app.use(bodyParser.json({limit: '32mb', extended: true}))

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Require our routes into the application.
require('./server/routes')(app);

module.exports = app;