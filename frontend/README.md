# Frontend
Página em ReactJS para consumir dados do servidor de login.
Contém duas páginas, uma pagina de boas-vindas e uma página de login.

Na página de login você é apresentado ou com uma frase dizendo que você não está logado, ou, se estiver logado, seu nome e seu nível de autorização.

## Para rodar

### Com npm
```
$ npm install
$ npm start
```

### Com Yarn
```
$ yarn install
$ yarn start
```

