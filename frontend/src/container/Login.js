import React, { Component } from "react";
import { Form, FormGroup, InputGroup, Card } from "react-bootstrap";
import { Redirect } from "react-router-dom";

// api
import api from "../api";

// Constants
import AppConfig from "../constants/AppConfig";

//Cookie
import Cookies from "universal-cookie";

const cookies = new Cookies();

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      loggedIn: false,
      hasError: false,
      errorMsg: ""
    };
  }

  componentWillMount() {
    // Get info from user on cookie
    if (cookies.get("token")) {
      this.setState({ loggedIn: true });
      this.props.login();
    }
  }

  errorOut(errorMsg) {
    // Sets up error message
    this.setState({ hasError: true, errorMsg });
    setTimeout(_ => this.setState({ hasError: false, errorMsg: "" }), 3000);
  }

  async login() {
    const { username, password } = this.state;

    try {
      const resp = await api.post(`${AppConfig.backend}/login`, {
        username,
        password
      });
      const { token } = resp.data;
      cookies.set("token", token, { path: "/" });

      this.setState({ loggedIn: true });
      this.props.login();
    } catch (e) {
      this.errorOut("Usário ou Senha incorreto");
    }
  }

  render() {
    const { username, password, loggedIn, hasError, errorMsg } = this.state;

    return (
      <div className="container" style={{ maxWidth: 300, marginTop: 30 }}>

        {loggedIn && <Redirect to="/" push />}

        <h1 className="text-center">Login</h1>
        <Form
          className="session-body"
          onSubmit={async e => {
            e.preventDefault();
            await this.login();
          }}
        >
          {/* =========  Usuário  =========== */}

          <FormGroup className="has-wrapper ">
            <InputGroup className="d-flex">
              <div className="input-group-prepend">
                <span className="input-group-text" id="username-addon">
                  <span className="fa fa-user" />
                </span>
              </div>
              <input
                id="username"
                value={username}
				        className="form-control"
                type="text"
                placeholder="Usuário"
                aria-describedby="username-addon"
                onChange={e => this.setState({ username: e.target.value })}
                required
              />
            </InputGroup>
          </FormGroup>

          {/* =========  Senha  =========== */}

          <FormGroup className="has-wrapper">
            <InputGroup className="d-flex">
              <div className="input-group-prepend">
                <span className="input-group-text" id="password-addon">
                  <span className="fa fa-key" />
                </span>
              </div>
              <input
                id="password"
                value={password}
                className="form-control input-lg"
                pattern=".*[A-Z]+.*[0-9]+.*"
                title="A senha deve conter pelo menos uma ocorrência de letra maiúscula e números"
                type="password"
                minLength={8}
                placeholder="Senha"
                aria-describedby="password-addon"
				        onChange={e => this.setState({ password: e.target.value })}
				        required
              />
            </InputGroup>
          </FormGroup>

          {/* =========  Enviar  =========== */}

          <FormGroup className="has-wrapper">
            <button type="submit" className="bnt btn-block">
              Entrar
            </button>
          </FormGroup>
        </Form>

        {/* =========  Erro  =========== */}

        {hasError && (
          <Card bg="danger" text="white">
            <Card.Header className="text-center">Erro no login</Card.Header>
            <Card.Body>
              <Card.Text>{errorMsg}</Card.Text>
            </Card.Body>
          </Card>
        )}
      </div>

    );
  }
}
export default Login;
