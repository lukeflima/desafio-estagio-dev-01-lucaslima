import React, { Component } from "react";
import { Redirect } from "react-router-dom";
//Cookie
import Cookies from "universal-cookie";

const cookies = new Cookies();

class Logoff extends Component {
  componentWillMount() {
    // Remove cookies if exists
    if (cookies.get("token")) {
      cookies.remove("token", { path: "/" });
    }
    this.props.logoff();
  }

  render() {
    return <Redirect to="/" push />;
  }
}
export default Logoff;
