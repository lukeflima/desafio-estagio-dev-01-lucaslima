import React, { Component } from "react";

//Cookie
import Cookies from "universal-cookie";

const cookies = new Cookies();

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      authcode: 0
    };
  }

  componentWillMount() {
    const token = cookies.get("token");
    if (token) {
      const { authcode, name } = JSON.parse(atob(token.split(".")[1]));
      this.setState({ authcode, name });
    }
  }
  render() {
    const { authcode, name } = this.state;

    return (
      <div className="container text-center" style={{ maxWidth: 700, marginTop: 30 }}>
        {(_ => {
          switch (authcode) {
            case 0:
              return (
                <h3>
                  Você não está conectado! Faça login para ter acesso a página.
                </h3>
              );
            case 1:
              return (
                <div>
                  <h3>Meu nome é {name}.</h3>
                  <h2>Eu sou usuário administrador do sistema.</h2>
                </div>
              );
            case 2:
              return (
                <div>
                  <h3>Meu nome é {name}.</h3>
                  <h2>Eu sou usuário comum do sistema.</h2>
                </div>
              );
            default:
              return null;
          }
        })()}
      </div>
    );
  }
}
export default Home;
