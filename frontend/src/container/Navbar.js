import React, { Component } from "react";
import { Navbar, Nav } from "react-bootstrap";

//css
import "./Navbar.css";

import AppConfig from "../constants/AppConfig";

class NavbarComponent extends Component {
  render() {
    const { active, loggedIn } = this.props;

    return (
      <Navbar bg="dark" variant="dark">
        <div className="container">
          <Navbar.Brand href="/">{AppConfig.brandName}</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link active={active === "/"} href="/">
              Início
            </Nav.Link>
            {!loggedIn && (
              <Nav.Link active={active === "/login"} href="/login">
                Login
              </Nav.Link>
            )}
            {loggedIn && (
              <Nav.Link active={active === "/logoff"} href="/logoff">
                Sair
              </Nav.Link>
            )}
          </Nav>
        </div>
      </Navbar>
    );
  }
}
export default NavbarComponent;
