import axios from 'axios';

import Cookies from 'universal-cookie';

const cookies = new Cookies();

const instance = axios.create({
  timeout: 180000,
  headers: {'authorization': `AAA ${cookies.get("token")}`}
});

export default instance