import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

//Bootsrap
import "./static/bootstrap.min.css";

// Custom component
import Home from "./container/Home";
import Login from "./container/Login";
import Logoff from "./container/Logoff";
import Navbar from "./container/Navbar";

//Cookie
import Cookies from "universal-cookie";
const cookies = new Cookies();

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loggedIn: false,
      pathname: window.location.pathname
    };
  }

  componentWillMount() {
    if (cookies.get("token")) {
      this.setState({ loggedIn: true });
    }
  }
  render() {
    const { loggedIn, pathname } = this.state;
    return (
      <Router>
        <Navbar active={pathname} loggedIn={loggedIn} />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route
            path="/login"
            exact
            render={_ => (
              <Login
                login={_ => this.setState({ loggedIn: true, pathname: "/" })}
              />
            )}
          />
          <Route
            path="/logoff"
            exact
            render={_ => (
              <Logoff
                logoff={_ => this.setState({ loggedIn: false, pathname: "/" })}
              />
            )}
          />
        </Switch>
      </Router>
    );
  }
}

export default App;
