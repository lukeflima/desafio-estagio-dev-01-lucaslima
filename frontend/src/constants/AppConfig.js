/**
 * App Config File
 */
const AppConfig = {
  backend: "http://localhost:5000", // Backend adress
  brandName: "PactoMais" // Brand Name
};

export default AppConfig;
